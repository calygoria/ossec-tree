import xmltodict
import json
import pprint
import os
import logging
import argparse

### Arguments
parser = argparse.ArgumentParser()
parser.add_argument("-p", "--path", default="/var/ossec/rules", help="Path to the folder containing OSSEC rules. Default value is '/var/ossec/rules'.")
parser.add_argument("-f", "--files", help="Specify one or more file(s) to parse, separated by a comma. Default behavior is to parse all files under the PATH directory.")
parser.add_argument("-o", "--orphans", help="Display rules that cannot be related to others. They will be reunited under an 'Orphans' category. Without this option, those rules will be ignored.", action="store_true")
parser.add_argument("-d", "--debug", help="Set logging level to DEBUG", action="store_true")
args = parser.parse_args()

### Logging
if args.debug:
    logging.basicConfig(level=logging.DEBUG)
    logging.debug('You set logging to DEBUG')
else:
    logging.basicConfig(level=logging.INFO)

### Path to OSSEC rules
logging.debug('Path to OSSEC rules is : ' + args.path)

### Variables
docs = []
first_level = []
first_level_rules = []
children_rules = []
ophans_rules = []
files = []
rule_counter = 0
counter = 0

### The function that recursively finds children
def get_children(listRule, node_id):
    global rule_counter
    # Find child nodes
    #logging.debug("Rule " + node_id + " is looking for its children...")
    children = []
    for rule in listRule:
        if 'if_matched_sid' in rule:
            if rule['if_matched_sid'] == node_id:
                #listRule.remove(rule)
                #logging.debug("(if_matched_sid) Rule " + node_id + " has a new child : " + rule['@id'])
                #logging.debug(listRule)
                rule_counter += 1
                child = {
                    'name' : rule['description'],
                    'nodeName' : rule['description'],
                    'rule_id' : rule['@id'],
                    'level' : rule['@level'],
                    'children' : get_children(listRule, rule['@id']),
                    "link" : {
                        "name" : rule['@id'],
                        "nodeName" : rule['@id'],
                        "direction" : "ASYN"
                    }
                }
                children.append(child)
        elif 'if_sid' in rule:
            if rule['if_sid'] == node_id:
                #listRule.remove(rule)
                #logging.debug("(if_sid) Rule " + node_id + " has a new child : " + rule['@id'])
                #logging.debug(listRule)
                rule_counter += 1
                child = {
                    'name' : rule['description'],
                    'nodeName' : rule['description'],
                    'rule_id' : rule['@id'],
                    'level' : rule['@level'],
                    'children' : get_children(listRule, rule['@id']),
                    "link" : {
                        "name" : rule['@id'],
                        "nodeName" : rule['@id'],
                        "direction" : "ASYN"
                    }
                }
                children.append(child)

        if 'if_sid' not in rule and 'if_matched_sid' not in rule:
            logging.debug("Who are you btw ?")
    return children

### Setting up the root of the JSON output
data = {
    'tree' : {
        "nodeName" : "OSSEC Rules",
        "name" : "OSSEC Rules",
        "rule_id" : "~",
        "level" : "root",
        "label" : "",
        "description" : "The OSSEC rules tree",
        "link" : {
                "name" : "Link NODE NAME 1",
                "nodeName" : "NODE NAME 1",
                "direction" : "ASYN"
            },
        "children" : []
        }
}

 ### Looking for files to parse
if args.files is not None:
    for file in args.files.split(","):
        files.append(file)
else:
    for file in os.listdir(args.path):
        files.append(file)

### Parsing files
for file in files:
    if file == 'rules_config.xml':
        pass
    else:

        cleanFiles = []
        logging.info('Parsing file : ' + file)
)
        with open(args.path + '/' + file) as fd:
            # Cleaning the files that are not valid XML (multiple root tags or invalid commentaries)
            cleanFile = ""
            for line in fd.readlines():
                if not line.startswith('<var') and not line.startswith('<!-- OSSEC Rules for Windows Firewall'):
                    cleanFile += line
                    if line.startswith('</group>'):
                        cleanFiles.append(cleanFile)
                        cleanFile = ""

            # Parsing XML -> Dict
            try:
                for myFile in cleanFiles:
                    docs.append(xmltodict.parse(myFile, process_namespaces=False,dict_constructor=dict))
            except:
                logging.error("Cannot parse file : " + file)


# Gathering first level rules, children rules and orphans rules
for doc in docs:
    for rule in doc['group']['rule']:
        if '@id' not in rule:
            pass
        elif 'decoded_as' in rule and not 'if_sid' in rule:
            counter += 1
            first_level_rules.append(rule)
        elif '@noalert' in rule:
            counter += 1
            first_level_rules.append(rule)
        elif 'if_sid' in rule:
            counter += 1
            children_rules.append(rule)
        elif 'if_matched_sid' in rule:
            counter += 1
            children_rules.append(rule)
        else:
            ophans_rules.append(rule)
            pass

# Parsing Dict -> JSON
for rule in first_level_rules:
    rule_counter += 1
    descriptionTmp = ""
    if 'description' in rule:
        descriptionTmp = rule['description']
    else:
        descriptionTmp = 'N/A'

    first_level.append({
        'name' : descriptionTmp,
        'nodeName' : descriptionTmp,
        'rule_id' : rule['@id'],
        'level' : 'root',
        'children' : get_children(children_rules, rule['@id']),
        "link" : {
            "name" : rule['@id'],
            "nodeName" : rule['@id'],
            "direction" : "ASYN"
        }
    })

# Sorting list by rule ID
first_level.sort(key = lambda x: int(x['rule_id']))

#logging.debug("Unparsed rules :")
#logging.debug(children_rules)
logging.info("Job is done.")
logging.info("Identified rule(s) :" + str(counter))
logging.info("Parsed rule(s) :" + str(rule_counter))
logging.info("Orphans rule(s) : " + str(len(ophans_rules)))

### Wiring first level items to the root
for item in first_level:
    data['tree']['children'].append(item)

### Writing output file
with open('data.json', 'w') as outFile:
    logging.info('Writing results in output file...')
    outFile.write(json.dumps(data, indent=4, sort_keys=True))
    logging.info('Writing successfull !')
