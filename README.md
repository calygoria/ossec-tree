# OSSEC Tree

A Python + JavaScript script to :
- parse the XML rules from OSSEC
- draw a tree of the rule dependencies in a web page using D3.js